﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayManager : MonoBehaviour
{
    // Start is called before the first frame update

    public List<PucketManager> puckets = new List<PucketManager>();
  
    public int pPucksCount =8 ;
    public int oPucksCount = 8;

    public GameObject win;
    public GameObject loose;

    private List<GameObject> playerSidePucets = new List<GameObject>();
    private List<GameObject> opponentSidePuckets = new List<GameObject>();

    public GameLevelsInfo gameLevelsInfo;
    public PlayerProfileInfo playerProfileInfo;

    private void Awake()
    {
        gameLevelsInfo = new GameLevelsInfo();
        playerProfileInfo = new PlayerProfileInfo();
        playerProfileInfo.playerData.gameLevel = 5;
        playerProfileInfo.SaveData();
    }

    void Update()
    {
        for (int i = 0; i < puckets.Count; i++)
        {
            if (puckets[i].onPlayerSide)
            {
                if (!playerSidePucets.Contains(puckets[i].gameObject))
                {
                    playerSidePucets.Add(puckets[i].gameObject);
                }

                if (opponentSidePuckets.Contains(puckets[i].gameObject))
                {
                    if (puckets[i].GetComponent<PucketManager>().isAISelected)
                    {
                        puckets[i].GetComponent<PucketManager>().isAISelected = false;
                        aiSelectedPucket = null;
                    }
                    opponentSidePuckets.Remove(puckets[i].gameObject);
                }
            }

            else if (puckets[i].onOpponentSide)
            {
                if (playerSidePucets.Contains(puckets[i].gameObject))
                {
                    playerSidePucets.Remove(puckets[i].gameObject);
                }

                if (!opponentSidePuckets.Contains(puckets[i].gameObject))
                {
                    opponentSidePuckets.Add(puckets[i].gameObject);
                }
            }
        }

        if (playerSidePucets.Count == 16)
        {
            Debug.LogError("Opponent Win");
            loose.SetActive(true);
        }
       
        if (opponentSidePuckets.Count == 16)
        {
            Debug.LogError("Player Win");
            win.SetActive(true);
        }
        if (opponentSidePuckets.Count > 0)
        {
             StartCoroutine(SelectAI());
        }
    }

    private void ReduceSpeed(GameObject gameObject)
    {
        if (gameObject!= null)
        {
                gameObject.GetComponent<Rigidbody>().velocity /= (3);
            //for (int i = 1; i < 20f; i++)
            //{
            //}
        }
    }

    public void CallAiPlay()
    {
        aiSelectedPucket = null;
    }

    private PucketManager aiSelectedPucket = null;

    IEnumerator  SelectAI()
    {
      
        yield return new WaitForSeconds(2.5f);
        int index = UnityEngine.Random.Range(0, opponentSidePuckets.Count);
        if (opponentSidePuckets.Count>0)
        {
            if (opponentSidePuckets[index] != null)
            {
                aiSelectedPucket = opponentSidePuckets[index].GetComponent<PucketManager>();
                aiSelectedPucket.isAISelected = true;
            }
        }
       
        StopAllCoroutines();
    }

    public void OnTapGameOver()
    {
        SceneManager.LoadScene("GameScene");
    }
}
