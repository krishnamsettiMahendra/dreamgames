﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[System.Serializable]
public class PlayerData
{
    public string playerName;
    public int playerLevel;
    public int gameLevel;
    public int rewards;
}

public class PlayerProfileInfo
{
    public PlayerData playerData;
    private TextAsset playerJsonAsset;

    public PlayerProfileInfo()
    {
        if (playerData == null)
        {
            playerJsonAsset = Resources.Load<TextAsset>("PlayerData");
            playerData = JsonUtility.FromJson<PlayerData>(playerJsonAsset.ToString());
            Debug.LogError(playerData);
        }
    }

    public void SaveData()
    {
        string jsonData = JsonUtility.ToJson(playerData);
        string filePath = Application.dataPath + "/Resources/PlayerData.json";

        if (File.Exists(filePath))
        {
            System.IO.File.WriteAllText(filePath, jsonData);
        }

    }
}
