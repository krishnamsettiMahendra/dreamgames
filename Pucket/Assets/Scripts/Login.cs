﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Login : MonoBehaviour
{
    public GameObject companyName;
    public GameObject gameName;
    public GameObject loginScene;
    void Start()
    {
        Invoke("EnableGameName", 2f);
    }

   public void EnableGameName()
    {
        companyName.SetActive(false);
        gameName.SetActive(true);
        Invoke("DisplayLogin", 2f);
    }

    public void DisplayLogin()
    {
        gameName.SetActive(false);
        loginScene.SetActive(true);
    }

}
