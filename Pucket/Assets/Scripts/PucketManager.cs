﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Assertions.Must;

public class PucketManager : MonoBehaviour
{
    public float forceMul = 190f;

    public LineRenderer playerSide;
    public LineRenderer opponentSide;
    
    public BoxCollider playerPlane;
    public BoxCollider opponentPlane;

    public String isInContact = null;
    public bool  onPlayerSide;
    public bool  onOpponentSide;
    public bool canMove;
    public bool isTouched;

    private Rigidbody rigidBody;
    private Vector3 defaultPosition;
    private Vector3 defaultPosition2;
    private float pucketWidth;

    #region privateProperties

    float yPosition;
    float zPosition;
    Vector3 offset;
    Camera mainCamera;
    bool dragging;
    #endregion

    //AI properties
    public Transform centerTransform;
    public float stretchForce;
    public float AIMovementSpeed = 10;
    public float xOffset = 1;
    private bool isAIMovingTocenter = false;
    private bool isAIShot = false;
    private Vector3 zMovement;
    public float AIShotForce = 175;
    [SerializeField]
    public UnityEvent OnBeginDrag;
    [SerializeField]
    public UnityEvent OnEndDrag;

    private void Awake()
    {
        //spring = transform.gameObject.GetComponent<SpringJoint>();
        rigidBody = GetComponent<Rigidbody>();
    }
    void Start()
    {
        defaultPosition = playerSide.GetPosition(2);
        defaultPosition2 = opponentSide.GetPosition(2);

        SphereCollider boxWidth = transform.GetComponent<SphereCollider>();
        //BoxCollider boxWidth = transform.GetComponent<BoxCollider>();
        pucketWidth = boxWidth.bounds.size.x;

        mainCamera = Camera.main;
        yPosition = mainCamera.WorldToScreenPoint(transform.position).y;
        zPosition = mainCamera.WorldToScreenPoint(transform.position).z;

        isTouched = false;
    }

    public bool isAISelected
    {
        get;
        set;
    }


    void Update()
    {
        if (isAISelected)
        {
            AiPlay();
        }
        
        if (dragging)
        {
            Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
            Vector3 positionChanged = mainCamera.ScreenToWorldPoint(Input.mousePosition + offset);
            RestrictPucketMovement(positionChanged);
        }
        

        if (isInContact == "Player")
           LineRendererUpdate(true);
        if (isInContact == "AI")
            LineRendererUpdate(false);


        if (playerPlane.bounds.Contains(this.transform.position))
        {
            onPlayerSide = true;
            onOpponentSide = false;
            if (isAIShot)
            {
                isAIShot = false;
            }
            if (isAIMovingTocenter)
            {
                isAIMovingTocenter = false;
            }
        }
        if(opponentPlane.bounds.Contains(this.transform.position))
        {
            isTouched = false;
            onOpponentSide = true;
            onPlayerSide = false;
            if (rigidBody.velocity.magnitude < 3 && isAIMovingTocenter)
            {
                if (isAIShot)
                {
                    isAIShot = false;
                    isAIMovingTocenter = false;
                    isAISelected = false;
                }
            }
        }
        if (canMove)
        {
            rigidBody.AddForce(transform.right * forceMul);
            canMove = false;
        }

        if (rigidBody.velocity.magnitude>0f)
        {
            rigidBody.velocity -= rigidBody.velocity * 0.85f*Time.deltaTime;
        }
        //if (transform.position.x > 0.21f)
        //{
        //    transform.position = new Vector3(0.1f, transform.position.y, transform.position.z);
        //}
            
    }

    public void RestrictPucketMovement( Vector3 position)
    {
        position.y = 0.55f;

        if (position.x < -1.27f)
            position.x = -1.27f;

        if (!isTouched)
        {
            if (position.x > -0.56f)
            {
                position.x = -0.56f;
            }
        }

        if (position.z < -0.35f)
            position.z = -0.35f;
        else if (position.z > 0.35f)
            position.z = 0.35f;

        transform.position = position;
    }

    private void OnMouseDown()
    {
        if (!dragging)
            BeginDrag();
    }

    private void OnMouseUp()
    {
        EndDrag();
    }

    public void BeginDrag()
    {
        if (onPlayerSide)
        {
            OnBeginDrag.Invoke();
            dragging = true;
            offset = mainCamera.WorldToScreenPoint(transform.position) - Input.mousePosition;
        }
    }

    public void EndDrag()
    {
        if (collidedObjct != null)
        {
           
            if (canMove == false)
            {
                canMove = true;
                collidedObjct = null;
            }
        }
        OnEndDrag.Invoke();
        dragging = false;
    }

    void LineRendererReset(bool player)
    {
        if(player)
        playerSide.SetPosition(1, defaultPosition);
        else
        opponentSide.SetPosition(1, defaultPosition2);
    }

    private GameObject collidedObjct = null;

    void OnTriggerEnter(Collider other)
    {
       // collidedObjct = null;
        if (other.gameObject.name == "Playerside")
        {
            collidedObjct = other.gameObject;
            isInContact = "Player";
            isTouched = true;
            LineRendererUpdate(true);
           
            if (!dragging)
            {
                rigidBody.velocity = Vector3.zero;
                Vector3 direction = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position) - Vector3.right;
                rigidBody.AddForce(Vector3.Reflect(direction, other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position))*15f);
            }
        }
 
        if (other.gameObject.name == "OpponentSide") 
        {
            isInContact = "AI";
            LineRendererUpdate(false);
            rigidBody.velocity = Vector3.zero;
            Vector3 direction = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position) - Vector3.right;
            rigidBody.AddForce(Vector3.Reflect(direction, other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) * 15f);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if(isInContact == "Player")
        LineRendererReset(true);
        else if (isInContact == "AI")
            LineRendererReset(false);
        //if (dragging)
        //{
        //canMove = false;
        //collidedObjct = null;

        //}
        isInContact = null;
    }
    void LineRendererUpdate(bool isplayer)
    {
        if (isplayer)
        {
            float tempx = transform.position.x;
            tempx = tempx - (pucketWidth /2);
            Vector3 newPosition = new Vector3(tempx, transform.position.y, transform.position.z);
            playerSide.SetPosition(1, newPosition);
        }
        else if(!isplayer )
        {
            float tempx = transform.position.x;
            tempx = tempx + (pucketWidth /2);
            Vector3 newPosition = new Vector3(tempx, transform.position.y, transform.position.z);
            opponentSide.SetPosition(1, newPosition);
            //Debug.Log(newPosition);

        }
    }

    public void AiPlay()
    {
        if (onOpponentSide)
        {
            if (rigidBody.velocity.magnitude < 3)
            {
                if (isAIMovingTocenter == false)
                {

                    if (Vector3.Distance(transform.position, centerTransform.position) < 0.05f)
                    {

                        isAIMovingTocenter = true;
                        zMovement = new Vector3(transform.position.x + xOffset, transform.position.y, transform.position.z);
                        Debug.LogError("Distance  " + zMovement);
                    }
                    else
                    {
//                        Debug.LogError("Distance  " + (Vector3.Distance(transform.position, centerTransform.position)));

                        Vector3 smoothMove = Vector3.MoveTowards(transform.position, centerTransform.position, Time.deltaTime * AIMovementSpeed);
                        rigidBody.MovePosition(new Vector3(smoothMove.x, transform.position.y, smoothMove.z));

                    }
                }

                if (isAIMovingTocenter &&(isAIShot == false))
                {
                    if (Vector3.Distance(transform.position, zMovement) < 0.3f)
                    {
                        isAIShot = true;
                        rigidBody.AddForce(-Vector3.right * AIShotForce);
                    }
                    else
                    {
                        Vector3 smoothMove = Vector3.MoveTowards(transform.position, zMovement, Time.deltaTime * AIMovementSpeed);
                        rigidBody.MovePosition(new Vector3(smoothMove.x, transform.position.y, smoothMove.z));
                    
                    }
                }
            }
        }
    }

}
