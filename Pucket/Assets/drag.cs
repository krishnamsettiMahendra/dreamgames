﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drag : MonoBehaviour
{

    private Vector3 mOffset;
    Plane objPlane;
    GameObject gameObj;

    Ray GenerateMouseRay()
    {
        Vector3 mouseFarPOs = new Vector3(Input.mousePosition.x,
                                           Input.mousePosition.y,
                                           Camera.main.farClipPlane);
        Vector3 mouseNearPOs = new Vector3(Input.mousePosition.x,
                                           Input.mousePosition.y,
                                           Camera.main.nearClipPlane);
        Vector3 mouseF = Camera.main.ScreenToWorldPoint(mouseFarPOs);
        Vector3 mouseN = Camera.main.ScreenToWorldPoint(mouseNearPOs);
        Ray mr = new Ray(mouseN, mouseF - mouseNearPOs);
        return mr;
    }
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Ray mouseRay = GenerateMouseRay();
            RaycastHit hit;

            if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit))
            {
                gameObj = hit.transform.gameObject;
                objPlane = new Plane(Camera.main.transform.forward * -1, gameObj.transform.position);

                Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                float rayDiatance;
                objPlane.Raycast(mRay, out rayDiatance);
                mOffset = gameObj.transform.position - mRay.GetPoint(rayDiatance);
            }
        }

        else if (Input.GetMouseButton(0) && gameObj)
        {
            Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            float rayDiatance;
            objPlane.Raycast(mRay, out rayDiatance);
            gameObj.transform.position = mRay.GetPoint(rayDiatance) + mOffset;
        }

        else if (Input.GetMouseButtonUp(0) && gameObj)
        {
            gameObj = null;
        }
    }
}
