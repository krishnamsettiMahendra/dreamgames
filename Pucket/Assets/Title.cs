﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour
{
    public GameObject gameName;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("ShowGameName", 2f);
    }

   public void ShowGameName()
    {
        gameName.SetActive(true);
        Invoke("StartGameScene", 2f);
    }

    public void StartGameScene()
    {
        SceneManager.LoadScene("HomeScene");
    }


}
