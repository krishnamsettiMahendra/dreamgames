﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

[System.Serializable]
public class LevelInfo
{
    public int noPuckets ;
    public string isBlackHole ;
    public string istTimeLimit ;
    public int timeLimit ;
    public string isholeFixed ;
    public string isBlackHoleFixed ;
    public int rewardCoins ;
    public int? noOfBlackHoles ;
}

[System.Serializable]
public class GameLevels
{
    public List<LevelInfo> gameLevels;
}


public class GameLevelsInfo
{
    public GameLevels gameLevels;

    public GameLevelsInfo()
    {
        if (gameLevels == null)
        {
            TextAsset jsonAsset = Resources.Load<TextAsset>("GameLevels");
            gameLevels = JsonUtility.FromJson<GameLevels>(jsonAsset.ToString());
            Debug.LogError(gameLevels);
        }
    }

    public void SaveData()
    {

    }
}
